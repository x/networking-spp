===============================
networking-spp (OpenStack)
===============================

Neutron ML2 mechanism driver for Soft Patch Panel

This provides ML2 mechanism driver and agent which makes high speed
communication using Soft Patch Panel (SPP) possible in the OpenStack
environment.

* Free software: Apache license
* Source: https://opendev.org/x/networking-spp/openstack
* Bugs: https://bugs.launchpad.net/networking-spp
