===============================
networking-spp
===============================

This project includes two VIM (Virtual Infrastructure Manager) integrations
of a high speed communication platform, Soft Patch Panel (SPP).

Neutron ML2 mechanism driver and agent under the ``/opentstack`` directory
enables SPP in OpenStack environments. The ``/devstack`` directory includes
the installation script for OpenStack.

Kubernetes Operator for SPP is under the ``operator`` directory enables
SPP in Container environments.

* Free software: Apache license
* Source: https://opendev.org/x/networking-spp
* Bugs: https://bugs.launchpad.net/networking-spp
