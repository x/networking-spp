// Package v1 contains API Schema definitions for the spp v1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=spp.spp
package v1
